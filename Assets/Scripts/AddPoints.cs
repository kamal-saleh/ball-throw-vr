﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddPoints : MonoBehaviour
{
    public Text score;

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Toy")
        {
            newspawner.Score += 1;
            score.text = newspawner.Score.ToString();
            Debug.Log(newspawner.Score);
        }
    }
}