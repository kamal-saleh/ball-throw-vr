﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newspawner : MonoBehaviour
{
    public static float Score;
    public int numberOfToys = 6;
    public int numberOfBalls = 5;
    public Transform[] spawnToylocations;
    public GameObject[] spawningToyObjects;
    public GameObject[] spawningToyObjectsClone;

    public Transform[] BallLocations;
    public GameObject[] spawningBallObjects;
    public GameObject[] spawningBallObjectsClone;

    private void Start()
    {
        spawningToyObjectsClone = spawningToyObjects;
        spawningBallObjectsClone = spawningBallObjects;
    }

    public void destroyObjects()
    {
        Debug.Log("PrintOnEnable: script was disabled");
        Debug.Log("destroy toys");
        Debug.Log("destroy balls");

    }

    public void spawnObjects()
    {
        Debug.Log("PrintOnEnable: script was enabled");
        destroyToys();
        destroyBalls();
        spawnThisToy();
        Debug.Log("spawn toys");
        spawnThisBall();
        Debug.Log("spawn balls");
    }


    void spawnThisToy()
    {
        for (int i = 0; i < numberOfToys; i++)
        {
            if (spawningToyObjects[i] != null)
            {
                spawningToyObjectsClone[i] = Instantiate(spawningToyObjects[i], spawnToylocations[i].transform.position, spawnToylocations[i].transform.rotation) as GameObject;
            }
        }
    }

    void destroyToys()
    {
        for (int i = 0; i < numberOfToys; i++)
        {
            Destroy(spawningToyObjectsClone[i]);
        }
    }

    void destroyBalls()
    {
        for (int i = 0; i < numberOfBalls; i++)
        {
            Destroy(spawningBallObjectsClone[i]);
        }
    }

    void spawnThisBall()
    {
        for (int i = 0; i < numberOfBalls; i++)
        {
            if (spawningBallObjects[0] != null)
            {
                spawningBallObjectsClone[i] = Instantiate(spawningBallObjects[0], BallLocations[i].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
            }
        }
    }
}